package com.example.assignmenttwelve.model

data class TransportationModel(
    val type:String,
    val organization:String,
    val address:String,
    val description:String?,
    val isDummy:Boolean,
    val hasDriver:Boolean,
    val internalTransportations:InternalTransportation
)

data class InternalTransportation(
    val id:String,
    val projectRequestId:String,
    val description:String?,
    val status:String,
    val pickUpLocation:PickUpLocation
)

data class PickUpLocation(
    val type:String,
    val coordinates:List<Double>

)