package com.example.assignmenttwelve

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.assignmenttwelve.databinding.ActivityMainBinding
import com.example.assignmenttwelve.model.TransportationModel
import com.google.gson.Gson
import java.nio.charset.Charset

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setData()

    }



    fun getJsonValue():TransportationModel
    {
        val jsonString = getJsonStringFromAssets()!!
        val transportation  = Gson().fromJson(jsonString,TransportationModel::class.java)

        return transportation
    }



    private fun getJsonStringFromAssets():String?
    {
        var jsonString:String? = null
        val charset:Charset = Charsets.UTF_8

        val aseetsJsonFile = assets.open("Transportation.json")
        val size = aseetsJsonFile.available()
        val buffer = ByteArray(size)

        aseetsJsonFile.read(buffer)
        aseetsJsonFile.close()
        jsonString = String(buffer,charset)

        return jsonString

    }

    private fun setData()
    {
        binding.tvTypeValue.text = getJsonValue().type
        binding.tvOrganizationValue.text = getJsonValue().organization
        binding.tvAddressValue.text = getJsonValue().address
        binding.tvDescriptionValue.text = getJsonValue().description
        binding.tvIsDummyValue.text = getJsonValue().isDummy.toString()
        binding.tvHasDriverValue.text = getJsonValue().hasDriver.toString()

        binding.tvIdValue.text = getJsonValue().internalTransportations.id
        binding.tvProjectRequestIdValue.text = getJsonValue().internalTransportations.projectRequestId

        binding.tvLocationTypeValue.text = getJsonValue().internalTransportations.pickUpLocation.type
        binding.tvFirstCoordinateValue.text = getJsonValue().internalTransportations.pickUpLocation.coordinates[0].toString()
        binding.tvSecondCoordinateValue.text = getJsonValue().internalTransportations.pickUpLocation.coordinates[1].toString()
    }
}